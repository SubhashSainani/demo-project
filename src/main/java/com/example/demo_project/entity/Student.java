package com.example.demo_project.entity;

public class Student {
    private Integer rollNumber;

    public Student() {
    }

    public Student(Integer rollNumber) {
        this.rollNumber = rollNumber;
    }

    public Integer getRollNumber() {
        return rollNumber;
    }

    public void setRollNumber(Integer rollNumber) {
        this.rollNumber = rollNumber;
    }
}
